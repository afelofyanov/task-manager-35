package ru.tsc.felofyanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.felofyanov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.felofyanov.tm.dto.request.UserLoginRequest;
import ru.tsc.felofyanov.tm.dto.response.UserLoginResponse;

public class AuthEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Test
    public void testAuth() {
        final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("test", "test"));
        Assert.assertNotNull(response);
        final String token = response.getToken();
        Assert.assertNotNull(token);
    }

    @Test(expected = Exception.class)
    public void testAuthNegative() {
        final UserLoginResponse response = authEndpoint.login(new UserLoginRequest("test2", "test"));
        Assert.assertNotNull(response);
    }
}
