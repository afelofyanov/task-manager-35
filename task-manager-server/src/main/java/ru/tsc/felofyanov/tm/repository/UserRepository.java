package ru.tsc.felofyanov.tm.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.IUserRepository;
import ru.tsc.felofyanov.tm.model.User;

import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null) return null;
        return models.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null) return null;
        return models.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null) return null;
        @Nullable final Optional<User> model = Optional.ofNullable(findByLogin(login));
        model.ifPresent(models::remove);
        return model.orElse(null);
    }
}
