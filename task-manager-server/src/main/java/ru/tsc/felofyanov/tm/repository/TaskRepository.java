package ru.tsc.felofyanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.ITaskRepository;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractUserOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @NotNull final String name) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return Collections.emptyList();
        return models.stream()
                .filter(task -> task.getProjectId() != null
                        && task.getUserId() != null
                        && projectId.equals(task.getProjectId())
                        && userId.equals(task.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Task create(final @Nullable String userId, final @NotNull String name, final @NotNull String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        task.setDescription(description);
        return add(task);
    }
}
