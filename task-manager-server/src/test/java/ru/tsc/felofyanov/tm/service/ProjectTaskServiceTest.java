package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.felofyanov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.felofyanov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.felofyanov.tm.exception.field.UserIdEmptyException;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.model.User;
import ru.tsc.felofyanov.tm.repository.ProjectRepository;
import ru.tsc.felofyanov.tm.repository.TaskRepository;

public class ProjectTaskServiceTest {

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final TaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Test
    public void bindTaskToProject() {
        @NotNull User user = new User();
        @NotNull Project project = projectRepository.create(user.getId(), "DEMO");
        @NotNull Task task = taskRepository.create(user.getId(), "MEGA");

        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(null, "test", "test"));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject("test", null, "test"));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject("test", "test", null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject("test", "test", "test"));

        Assert.assertNull(task.getProjectId());
        projectTaskService.bindTaskToProject(user.getId(), project.getId(), task.getId());
        Assert.assertNotNull(task.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        @NotNull User user = new User();
        @NotNull Project project = projectRepository.create(user.getId(), "DEMO");
        @NotNull Task task = taskRepository.create(user.getId(), "MEGA");
        task.setProjectId(project.getId());

        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(null, "test", "test"));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject("test", null, "test"));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject("test", "test", null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.unbindTaskFromProject("test", "test", "test"));

        Assert.assertNotNull(task.getProjectId());
        projectTaskService.unbindTaskFromProject(user.getId(), project.getId(), task.getId());
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void removeProjectById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.removeProjectById(null, "test"));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById("test", null));

        @NotNull User user = new User();
        @NotNull Project project = projectRepository.create(user.getId(), "DEMO");
        @NotNull Task task = taskRepository.create(user.getId(), "MEGA");
        task.setProjectId(project.getId());

        Assert.assertNotNull(taskRepository.findOneById(task.getId()));
        Assert.assertNotNull(projectRepository.findOneById(project.getId()));

        projectTaskService.removeProjectById(user.getId(), project.getId());

        Assert.assertNull(taskRepository.findOneById(task.getId()));
        Assert.assertNull(projectRepository.findOneById(project.getId()));
    }
}
