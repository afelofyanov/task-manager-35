package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.exception.entity.ModelNotFoundException;
import ru.tsc.felofyanov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.felofyanov.tm.exception.field.*;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.repository.TaskRepository;

import java.util.List;

public class TaskServiceTest {

    @NotNull
    private final TaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final TaskService taskService = new TaskService(taskRepository);

    @Test
    public void add() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add(null, null));
        Assert.assertThrows(ModelNotFoundException.class, () -> taskService.add("123", null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.add("", null));

        @NotNull Task task = new Task();
        taskService.add("123", task);
        Assert.assertFalse(taskService.findAll().isEmpty());

        Assert.assertNotNull(taskService.findAll(task.getUserId()));
    }

    @Test
    public void updateById() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateById(null, "test", "test", "test"));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.updateById("test", null, "test", "test"));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateById("test", "test", null, "test"));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateById("test", "test", "test", null));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.updateById("test", "test", "test", "test"));

        @NotNull Task task = taskService.create("test", "DEMKA", "test");
        Assert.assertFalse(taskService.findAll().isEmpty());

        Assert.assertNotNull(taskService.updateById("test1", task.getId(), "No demo", "tester"));
    }

    @Test
    public void updateByIndex() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.updateByIndex(null, 0, "test", "test"));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex("test", null, "test", "test"));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.updateByIndex("test", 0, null, "test"));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.updateByIndex("test", 0, "test", null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.updateByIndex("test", -1, "test", "test"));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> taskService.updateByIndex("test", 1, "test", "test"));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> taskService.updateByIndex("test", 0, "test", "test"));

        taskService.create("test", "DEMKA", "test");
        Assert.assertFalse(taskService.findAll().isEmpty());

        Assert.assertNotNull(taskService.updateByIndex("test1", 0, "No demo", "tester"));
    }

    @Test
    public void changeTaskStatusById() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusById(null, "test", Status.NOT_STARTED));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeTaskStatusById("test", null, Status.NOT_STARTED));
        Assert.assertThrows(StatusEmptyException.class, () -> taskService.changeTaskStatusById("test", "test", null));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.changeTaskStatusById("test", "test", Status.NOT_STARTED));

        @NotNull Task task = taskService.create("test", "DEMKA", "test");
        Assert.assertFalse(taskService.findAll().isEmpty());

        Assert.assertNotNull(taskService.changeTaskStatusById("test1", task.getId(), Status.IN_PROGRESS));
    }

    @Test
    public void changeTaskStatusByIndex() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeTaskStatusByIndex(null, 0, Status.NOT_STARTED));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex("test", null, Status.NOT_STARTED));
        Assert.assertThrows(StatusEmptyException.class, () -> taskService.changeTaskStatusByIndex("test", 0, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> taskService.changeTaskStatusByIndex("test", -1, Status.NOT_STARTED));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> taskService.changeTaskStatusByIndex("test", 1, Status.NOT_STARTED));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> taskService.changeTaskStatusByIndex("test", 0, Status.NOT_STARTED));

        taskService.create("test", "DEMKA", "test");
        Assert.assertFalse(taskService.findAll().isEmpty());

        Assert.assertNotNull(taskService.changeTaskStatusByIndex("test1", 0, Status.IN_PROGRESS));
    }

    @Test
    public void create() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(null, ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(null, "test"));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(null, null));

        Assert.assertThrows(NameEmptyException.class, () -> taskService.create("", null));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create("test", null));

        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.create("test", "test", null));

        taskService.create("test", "test");
        Assert.assertFalse(taskService.findAll().isEmpty());
        @Nullable List<Task> serviceTest = taskService.findAll("test1");
        Assert.assertEquals(0, serviceTest.size());

        serviceTest = taskService.findAll("test");
        Assert.assertEquals(1, serviceTest.size());
    }

    @Test
    public void findAllByProjectId() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAllByProjectId(null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findAllByProjectId(null, "test"));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> taskService.findAllByProjectId("123", null));

        @NotNull Task task = new Task();
        task.setProjectId("11111");
        task.setUserId("test");
        taskService.add(task);
        Assert.assertFalse(taskService.findAll().isEmpty());

        @Nullable List<Task> serviceTest = taskService.findAllByProjectId("123", "test");
        Assert.assertEquals(0, serviceTest.size());

        serviceTest = taskService.findAllByProjectId("test", task.getProjectId());
        Assert.assertEquals(1, serviceTest.size());
    }
}
