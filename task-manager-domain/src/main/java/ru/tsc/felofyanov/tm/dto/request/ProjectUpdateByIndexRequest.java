package ru.tsc.felofyanov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectUpdateByIndexRequest extends AbstractIndexRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public ProjectUpdateByIndexRequest(
            @Nullable String token,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) {
        super(token, index);
        this.name = name;
        this.description = description;
    }
}
